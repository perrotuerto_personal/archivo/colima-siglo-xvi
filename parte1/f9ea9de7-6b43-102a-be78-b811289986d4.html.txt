[]

LA VILLA DE COLIMA DE LA NUEVA ESPAÑA
SIGLO XVI

VOLUMEN I      (CAJAS 1-11)

[]

Inicio | Búsquedas | Siglas y bibliografía | Cartografía | Créditos | Más información

[]

Búsqueda rápida

+:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:+------------------------------------------:+
| Fecha:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Ubicación física del documento original:  |
| 1575 Julio, 3                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | CAJA A-6, EXP. 8, FF. 24-30. (1)          |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Archivo Histórico del Municipio de Colima |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+
| 192. 1575. Julio, 3.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| Pero Ruiz de Vilches, en nombre de Pedro de Gamboa, vecino de México, contra Julián de Frías, defensor de los bienes del difunto Bartolomé de Vilches.                                                                                                                                                                                                                                                                                                                                                                                               |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| Ante el alcalde ordinario Cristóbal de Solórzano, Pero Ruiz de Vilches, en el pleito que trata en nombre de Pedro de Gamboa, vecino de la Ciudad de México, contra los bienes de Bartolomé de Vilches, difunto, y Julián de Frías, defensor de los dichos bienes, por el pago de una obligación de plazo vencido, dice que Julián de Frías niega dicha obligación de pago, alegando que en ésta dice "Bartolomé Ruiz" y no "Bartolomé de Vilches", lo cual no es razón suficiente para dejar de pagar.                                               |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| Pero Ruiz de Vilches hizo presentación de testigos. El primero de ellos a Juan Ramírez de Alarcón, quien aseguró haber conocido de muchos años a Bartolomé Ruiz de Vilches, y que éste le había dicho que "en la Ciudad de Guadalaxara y en otras partes donde había vivido e residió antes que viniese a esta Villa, le llamaban Bartolomé de Vilches", y no Bartolomé Ruiz, "que era su propio nombre". También atestigua que el difunto le dijo que lo contenido en la escritura en cuestión se lo había comprado a Gamboa obligándose a pagarle. |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| El segundo testigo presentado fue Gonzalo de Herrera, residente en esta Villa de Colima, quien dijo haber conocido de muchos años antes de morir a Bartolomé Ruiz de Vilches, y que estando este testigo un día en casa del susodicho, éste le dijo que su nombre propio era Bartolomé Ruiz y no Bartolomé de Vilches, y que este sobrenombre se lo había puesto después.                                                                                                                                                                            |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| Caja A–6, exp. 8, ff. 24-30.^([1])                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| ---------------                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |                                           |
| (1) De las ff. 24-28 sólo se conserva el margen.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |                                           |
+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+-------------------------------------------+

Documento(s) original(es)

Archivo no encontrado (CAJAS/CAJA6/EXP8/024F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/024V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/025F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/025V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/026F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/026V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/027F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/027V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/028F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/028V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/029V.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/030F.jpg)

Archivo no encontrado (CAJAS/CAJA6/EXP8/030V.jpg)

[]
29F

Proyecto interinstitucional del Archivo Histórico del Municipio de Colima y la Universidad de Colima,
realizado bajo los auspicios del fondo Ramón Álvarez-Buylla de Aldana.

------------------------------------------------------------------------

ISBN 978-968-7412-85-6
® Derechos Reservados 2008. Archivo Histórico del Municipio de Colima.

1,078 visitas
