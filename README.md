---
title: "Descarga automatizada de La Villa de Colima de la Nueva España, siglo XVI"
tags: ruweb, programación literaria, wiki, descarga, colima, siglo xvi
---

# Descarga automatizada de la Villa de Colima de la Nueva España, siglo XVI

Esta receta descarga el fondo documental del siglo XVI del Archivo de la Villa de Colima. Este comprende tres sitios hechos con la misma plataforma:

1. Cajas 1--11:

```ruby _parte1
http://casadelarchivo.colima.gob.mx/sigloxvi/index.php?buscador
```

2. Cajas 12--20:

```ruby _parte2
http://casadelarchivo.colima.gob.mx/sigloxvi_2/inicio.php?buscador
```


3. Cajas 21--30:

```ruby _parte3
http://casadelarchivo.colima.gob.mx/sigloxvi_3/inicio.php?buscador
```

:::warning
Aún no están disponibles las cajas 31--35, que son las últimas de este fondo.
:::

## Requisitos

Los siguientes elementos son necesarios:

* Sistema operativo basados en [Unix](https://es.wikipedia.org/wiki/Unix#Implementaciones_m%C3%A1s_importantes) como [GNU/Linux](https://es.wikipedia.org/wiki/GNU/Linux)
* [Ruby](https://www.ruby-lang.org) >= 2.5.1
* Gema [RuWEB](https://rubygems.org/gems/ruweb)
* Gema [HTTParty](https://www.johnnunemaker.com/httparty/)
* Gema [Nokogiri](https://nokogiri.org/)
* Gema [HtmlBeautifier](https://github.com/threedaymonk/htmlbeautifier)
* [Iconv](https://en.wikipedia.org/wiki/Iconv)
* [Pandoc](https://pandoc.org/)

## Procedimiento

Directorio destino:

```sh _dir
colima-siglo_xvi
```

Gemas a utilizar en Ruby:

```ruby=
require 'httparty'
require 'nokogiri'
require 'htmlbeautifier'
```

Aviso de inicio de descarga:

```ruby=+
puts "\n--- Descargando ---"
```

Variables a utilizar en este ejercicio:

```ruby=+
dir  = %Q{_dir}
urls = %W{_parte1 _parte2 _parte3}
```

Llamado al _[método `mkdir`](https://pad.programando.li/ruweb:cli:mkdir/download).

Generación de directorio:

```ruby=+
mkdir(dir)
```

Iteración de cada fondo:

```ruby=+
urls.each_with_index do |url, i|
  puts "\nURL: #{url}\nParte: #{i+1}"
  part = "#{dir}/parte#{i+1}/"
  site = HTTParty.get(url)
  html = Nokogiri::HTML(site.body)
  vals = html.search('option[value]')
  mkdir(part)
  _beautify
  _progress_bar
  _ids
  _download
end
```

Obtención de id de los expedientes:

```ruby= _ids
case i
when 0
  vals = vals.map{|e| e['value']}.reject{|e| e.length != 36}
when 1
  vals = Array.new(381) {|i| (i+1).to_s }
when 2
  vals = Array.new(407) {|i| (i+1).to_s }
end
```

Descarga de cada expediente:

```ruby= _download
vals.each_with_index do |record, j|
  puts "#{status(vals.length, j+1)} Expediente #{record}"
  record_url = "#{url}&show=#{record}"
  site       = HTTParty.get(record_url)
  content    = HtmlBeautifier.beautify(site.body)
  record     = beautify(vals.length, j+1) if i > 0
  File.write("#{part}#{record}.html", content)
end
```

Embellece número:

```ruby= _beautify
def beautify(total, current)
  zeros = total.to_s.length - current.to_s.length
  "#{'0'*zeros}#{current}"
end
```

Muestra la barra de progreso:

```ruby= _progress_bar
def status(total, current)
  "[#{beautify(total, current)}/#{total}]"
end
```

Arregla la codificación de los documentos:

```sh=
printf "\n--- Arreglando codificación ---\n"
for file in _dir/**/*.html
do
  iconv -f ISO-8859-1 -t utf-8 $file -o $file.new
  mv $file.new $file
done
```

Convierte a texto plano:

```sh=+
printf "\n--- Convirtiendo a texto plano ---\n"
for file in _dir/**/*.html
do
  pandoc -f html -t plain --wrap=none $file -o $file.txt
done
```